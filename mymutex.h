#include "myqueue.h"
#include "myatomic.h"

#ifndef MYMUTEX_H
#define MYMUTEX_H

#define MYTHREAD_MUTEX_INITIALIZER {1, NULL, 0}
#define MYTHREAD_MUTEX_DEINITIALIZER {0xdeadbeef, NULL, 0}

#define TTS_THRESHOLD 100
#define VALID_MAGIC 0xdeadbeef

#define STATE_BLOCKED 0x1

typedef struct mythread_mutex
{
  unsigned int valid;

  mythread_queue_t q;

  char lock;
}mythread_mutex_t;

typedef struct mythread_mutexattr
{
}mythread_mutexattr_t;

int mythread_mutex_init(mythread_mutex_t *mutex, mythread_mutexattr_t *attr);
int mythread_mutex_destroy(mythread_mutex_t *mutex);
int mythread_mutex_lock(mythread_mutex_t *mutex);
int mythread_mutex_trylock(mythread_mutex_t *mutex);
int mythread_mutex_unlock(mythread_mutex_t *mutex);

int test_and_test_set_lock(char *lock);
void test_and_test_and_set_unlock(char *lock);

#endif
