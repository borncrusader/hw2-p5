#include "mythread.h"
#include "mymutex.h"

typedef struct 
{
}mythread_condattr_t;

typedef struct
{
  int valid;
	mythread_queue_t condition_queue;
}mythread_cond_t;

int mythread_cond_destroy(mythread_cond_t *cond);

int mythread_cond_init(mythread_cond_t *cond,const mythread_condattr_t *attr);

int mythread_cond_wait(mythread_cond_t *cond, mythread_mutex_t *mutex);

int mythread_cond_broadcast(mythread_cond_t *cond);

int mythread_cond_signal(mythread_cond_t *cond);

