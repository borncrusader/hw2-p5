#include <stdio.h>
#include "mythread.h"
#include "mymutex.h"
#include <string.h>

mythread_queue_t q;
mythread_mutex_t m = MYTHREAD_MUTEX_INITIALIZER;

int glob;

void* thr_fn(void *args)
{
  long id = (long)args;
  char buf[100];
  
  sprintf(buf, "thread %ld\n", id);
  write(1, buf, strlen(buf));

  mythread_mutex_lock(&m);
  sprintf(buf, "locked %ld\n", id);
  write(1, buf, strlen(buf));
  //sleep(1);
  glob++;
  sprintf(buf, "unlocked %ld\n", id);
  write(1, buf, strlen(buf));
  mythread_mutex_unlock(&m);

  mythread_exit(NULL);
}

int main()
{
  long i;
  mythread_t t[10];

  mythread_setconcurrency(5);

  for(i=0; i<10; i++)
    mythread_create(&t[i], NULL, thr_fn, (void*)i);

  mythread_mutex_lock(&m);
  write(1, "main locks and yields!\n", 23);
  mythread_yield();
  //sleep(1);
  glob++;
  write(1,"in main!\n", 9);
  mythread_mutex_unlock(&m);

  printf("value of glob is %d\n", glob);
  mythread_exit(NULL);

  return 0;
}
