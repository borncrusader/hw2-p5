CC     = gcc
CFLAGS = -g

mytest: mutex_test.c p5.c mymutex.o mycond.o
	$(CC) $(CFLAGS) p5.c mymutex.o mythread.a mycond.o -o mytest

mymutex.o: mymutex.h mymutex.c 
	$(CC) $(CFLAGS) -c mymutex.c -o mymutex.o

mycond.o: mycond.h mycond.c 
	$(CC) $(CFLAGS) -c mycond.c -o mycond.o

clean:
	rm -rf mytest mut_test *.o
