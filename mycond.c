#include <errno.h>
#include "mycond.h"

int mythread_cond_destroy(mythread_cond_t *cond)
{
  if(!cond) return EINVAL;

  if(cond->condition_queue) return EBUSY;

  if(cond->valid == 1) {
    cond->valid = VALID_MAGIC;
  } else {
    return EINVAL;
  }

  return 0;
}
       
int mythread_cond_init(mythread_cond_t *cond,const mythread_condattr_t *attr)
{
	if( cond == NULL) 
		return EINVAL;

  cond->valid = 1;
	cond->condition_queue = NULL;

	return 0;
}

int mythread_cond_wait(mythread_cond_t *cond, mythread_mutex_t *mutex)
{
  mythread_t self = mythread_self();

  if(!cond || !mutex) return EINVAL;

	mythread_enter_kernel(); 
	mythread_block_phase1(&(cond->condition_queue), STATE_BLOCKED);
	
	// release the mutex and block 
	mythread_mutex_unlock(mutex);
	mythread_enter_kernel();
  if(self->state & STATE_BLOCKED) {
	  mythread_block_phase2(); 
  } else {
    mythread_leave_kernel();
  }
	mythread_mutex_lock(mutex); 

  return 0;
}

int mythread_cond_broadcast(mythread_cond_t *cond)
{
	mythread_queue_t queue_head;
	mythread_queue_t cur;

  if(!cond) return EINVAL;

  queue_head = cond->condition_queue;
  cur = queue_head;
	do
	{
		mythread_enter_kernel(); 
		mythread_unblock(&(cur), STATE_BLOCKED);
	}while( cur != queue_head && cur != NULL);

	return 0;
}

int mythread_cond_signal(mythread_cond_t *cond)
{
  if(!cond) return EINVAL;

	mythread_enter_kernel(); 
	mythread_unblock(&(cond->condition_queue), STATE_BLOCKED); 

  return 0;
}
