#include "string.h"
#include <stdio.h>
#include "mythread.h"
#include "mycond.h"
#include "mymutex.h"
#include <string.h>

mythread_queue_t q;
mythread_mutex_t m = MYTHREAD_MUTEX_INITIALIZER;
int cond_met = 0;
mythread_cond_t count_threshold_cv;
mythread_mutex_t count_mutex;

int workToDo = 0;
mythread_cond_t cond;
mythread_mutex_t mutex = MYTHREAD_MUTEX_INITIALIZER;
int glob;
void print(char* str)
{
	int len = strlen(str);
	write(1, str, len);
}

void* thr_fn(void *args)
{
  long id = (long)args;
  char buf[100];
  
  sprintf(buf, "thread %ld\n", id);
  write(1, buf, strlen(buf));

  mythread_mutex_lock(&m);
  sprintf(buf, "locked %ld\n", id);
  write(1, buf, strlen(buf));
  //sleep(1);
  glob++;
  sprintf(buf, "unlocked %ld\n", id);
  write(1, buf, strlen(buf));
  mythread_mutex_unlock(&m);

  mythread_exit(NULL);
}

void *threadfunc(void *parm)
{
  int           rc;
  char buff[100];
  print("signal thread func\n"); 
  {
    rc = mythread_mutex_lock(&mutex);
    print("got the mutex\n");
    
      print("SignalTest:waiting on Condition \n");
      rc = mythread_cond_wait(&cond, &mutex);
      print("returned from mythread_cond_wait signalling other thread()\n");
      mythread_cond_signal(&cond);

    print("unlocking the mutex, setting workTodo = 1\n");
    workToDo = 1;

    rc = mythread_mutex_unlock(&mutex);
  }
  return NULL;
}
void* waiter(void* args)
{
	print("waiter scheduled, waiting on lock\n"); 
	while(!cond_met)
	{
		mythread_mutex_lock(&count_mutex);
		print("waiter got the mutex, waiting on condition variable\n"); 
	        mythread_cond_wait(&count_threshold_cv, &count_mutex);
	} 
	print("got broadcast signal from main ,unlocking mutex\n"); 
        mythread_mutex_unlock(&count_mutex);
	mythread_exit(NULL); 
}

	
int main()
{
  long i;
  
  mythread_t t[5],tm[10];
  mythread_t threadid[2];
  mythread_setconcurrency(6);
  print("********* Mutex test:Main creates 10 threads, pause a bit *************\n");
  for(i=0; i<10; i++)
    mythread_create(&tm[i], NULL, thr_fn, (void*)i);
  mythread_mutex_lock(&m);
  write(1, "main locks,pauses a bit and updates the global!\n", 23);
  sleep(3);
  glob++;
  mythread_mutex_unlock(&m);  

  printf("\nvalue of glob is %d\n", glob);


  printf("\n\n\n\n\n********Signal test:Create 2 threads, pause a bit *******\n");
  for(i=0; i<2; ++i) 
    mythread_create(&threadid[i], NULL, threadfunc, NULL);
  sleep(5);
  mythread_mutex_lock(&mutex);
  workToDo = 1;
  mythread_cond_signal(&cond);
  mythread_mutex_unlock(&mutex);


  for(i=0; i<5; i++)
    mythread_create(&t[i], NULL, waiter, (void*)i);
  print("\n\n\n\n\n**********Broadcast test:main create 5 threads, pause a bit**************\n");
  sleep(5);
  cond_met = 1;
  mythread_mutex_lock(&count_mutex);
  mythread_cond_broadcast(&count_threshold_cv);
  print("main broadcasted\n");
  mythread_mutex_unlock(&count_mutex);

  mythread_exit(NULL);

  return 0;
}
