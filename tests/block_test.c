#include <stdio.h>
#include "../mythread.h"

mythread_queue_t q;

void* fn(void *args)
{
  printf("thread called\n");
  mythread_block(&q, 0);
  printf("thread unblocked\n");

  mythread_exit(NULL);
}

int main()
{
  mythread_t t1;

  mythread_create(&t1, NULL, fn, NULL);
  printf("main yields\n");
  mythread_yield();
  printf("main returns\n");
  //mythread_unblock(&q, 0);

  mythread_exit(NULL);

  return 0;
}
