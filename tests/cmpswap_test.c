#include <stdio.h>
#include "../myatomic.h"

int main()
{
  int val = 0;
  int ret;

  ret = compare_and_swap(&val, 1, 0);
  printf("%d, %d\n", ret, val);
  ret = compare_and_swap(&val, 2, 0);
  printf("%d, %d\n", ret, val);
  ret = compare_and_swap(&val, 2, 1);
  printf("%d, %d\n", ret, val);

  return 0;
}
