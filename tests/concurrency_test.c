#include <stdio.h>
#include "../mythread.h"

mythread_queue_t q;

void* fn(void *args)
{
  printf("thread\n");
  sleep(1);

  mythread_exit(NULL);
}

int main()
{
  int i;
  mythread_t t[10];

  mythread_setconcurrency(5);

  for(i=0; i<10; i++)
    mythread_create(&t[i], NULL, fn, NULL);

  printf("main yields\n");
  mythread_yield();
  printf("main returns\n");

  mythread_exit(NULL);

  return 0;
}
