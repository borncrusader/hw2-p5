#include <errno.h>
#include "mymutex.h"

int mythread_mutex_init(mythread_mutex_t *mutex, mythread_mutexattr_t *attr)
{
  if(!mutex) return EINVAL;

  // init already init-ed mutex??
  mutex->valid = 1;
  mutex->q = NULL;
  mutex->lock = 0;

  return 0;
}

int mythread_mutex_lock(mythread_mutex_t *mutex)
{
  if(!mutex) return EINVAL;

  // Invoke Test and Test and Lock 
  while(!test_and_test_and_set_lock(&mutex->lock)) {
    mythread_enter_kernel();
    if(mutex->lock) {
      mythread_block(&mutex->q, 0);
    } else {
      mythread_leave_kernel();
    }
  }

  return 0;
}

int mythread_mutex_trylock(mythread_mutex_t *mutex)
{
  if(!mutex) return EINVAL;

  if(mutex->lock) {
    // check if lock is taken, if yes, return EBUSY
    return EBUSY;
  }

  mythread_mutex_lock(mutex);

  return 0;
}

int mythread_mutex_unlock(mythread_mutex_t *mutex)
{
  if(!mutex) return EINVAL;

  // check whether it is the same fellow who locked??
  if(!mutex->lock) {
    return EINVAL;
  }

  test_and_test_and_set_unlock(&mutex->lock);

  // check if lock is taken, if yes, unblock it
  mythread_enter_kernel();
  //printf("\nUnblocked Tid:%lu\n",mythread_self());
  mythread_unblock(&mutex->q, 0);

  return 0;
}

int mythread_mutex_destroy(mythread_mutex_t *mutex)
{
  if(!mutex) return EINVAL;

  if(mutex->lock) {
    return EBUSY;
  }

  if(mutex->valid == 1) {
    mutex->valid = VALID_MAGIC;
  } else {
    return EINVAL;
  }

  return 0;
}

// Test and test and set lock based on Compare and swap
int test_and_test_and_set_lock(char *lock)
{
    int retry;

    while(1) {
      retry = 1;
      while(retry <= TTS_THRESHOLD)
      {
        if(!(*lock))
        {
          //printf("\nBreaking--retry:%d",retry);
          break;
        }

        //printf("\nRetrying--retry:%d",retry);
        retry++;
      }
      if(retry > TTS_THRESHOLD) {
        return 0;
      }

      if(compare_and_swap(lock, 1, 0) == 0)
      {
        //Set the lock
        //printf("\nSetting lock Tid:%lu\n",mythread_self());
        //mutex->lock = 1;
        break;
      }
    }
    
    return 1; 
}

void test_and_test_and_set_unlock(char *lock)
{
  *lock = 0;
}
